local null_ls = require("null-ls")

local sources = {
    null_ls.builtins.formatting.eslint,
    null_ls.builtins.diagnostics.eslint.with({
        method = null_ls.methods.DIAGNOSTICS_ON_SAVE,
    }),
}

null_ls.setup({ sources = sources })
