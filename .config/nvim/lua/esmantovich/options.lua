vim.opt.keymap="russian-jcukenwin"
vim.opt.iminsert=0
vim.opt.imsearch=0
vim.opt.langmap="ФИСВУАПРШОЛДЬТЩЗЙКЫЕГМЦЧНЯ;ABCDEFGHIJKLMNOPQRSTUVWXYZ,фисвуапршолдьтщзйкыегмцчня;abcdefghijklmnopqrstuvwxyz"

vim.opt.termguicolors = true                    -- set term gui colors (most terminals support this)
vim.opt.backup = false                          -- creates a backup file
vim.opt.swapfile = false                        -- creates a swapfile
vim.opt.fileencoding = "utf-8"                  -- the encoding written to a file
vim.opt.clipboard = "unnamedplus"               -- allows neovim to access the system clipboard
vim.opt.mouse = "a"                             -- allow the mouse to be used in neovim
vim.opt.showmode = false
vim.opt.undofile = true                         -- enable persistent undo
vim.opt.writebackup = false                     -- if a file is being edited by another program (or was written to file while editing with another program), it is not allowed to be edited
vim.opt.wrap = false


-- Tabs
vim.opt.expandtab = true                        -- convert tabs to spaces
vim.opt.shiftwidth = 4                          -- the number of spaces inserted for each indentation
vim.opt.tabstop = 4                             -- insert 4 spaces for a tab
vim.opt.smarttab = true
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4


-- Line numbers
vim.opt.number = true                           -- set numbered lines
vim.opt.relativenumber = true                  -- set relative numbered lines
vim.opt.numberwidth = 1                         -- set number column width to 1 {default 4}


-- Search
vim.opt.hlsearch = false                         -- highlight all matches on previous search pattern
vim.opt.ignorecase = true                       -- ignore case in search patterns
vim.opt.incsearch = true
vim.opt.smartcase = true                        -- smart case


-- Comfy
vim.opt.scrolloff = 8                           -- Auto scroll when reaching end of the screen
vim.opt.sidescrolloff = 8
vim.opt.splitbelow = true                       -- force all horizontal splits to go below current window
vim.opt.splitright = true                       -- force all vertical splits to go to the right of current window


-- Completion
vim.opt.completeopt = { "menuone", "noselect" } -- mostly just for cmp
vim.opt.pumheight = 10                          -- pop up menu height


-- Indenting
vim.opt.autoindent = true                      -- make indenting smarter again
-- Go do not indent line to the beginning when typing :
vim.cmd [[
    autocmd FileType go setlocal indentkeys-=<:>
    autocmd FileType go setlocal indentkeys-=:
]]


-- Cursor settings for base16-ashes theme
vim.cmd[[highlight Cursor guifg=white guibg=black]]
vim.cmd[[highlight iCursor guifg=white guibg=#a58dbd]]
vim.opt.guicursor = "n-v-c-sm:block-blinkon0-Cursor,i-ci-ve:iCursor-blinkwait300-blinkon200-blinkoff150"


-- ВОЗМОЖНО ВЕРНУТЬ 300 ЕСЛИ БУДУТ ПРОБЛЕМЫ!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
-- vim.opt.updatetime = 300                        -- faster completion (4000ms default)
vim.opt.updatetime = 4000                        -- faster completion (4000ms default)
vim.opt.timeoutlen = 1000                       -- time to wait for a mapped sequence to complete (in milliseconds)


-- ???
vim.opt.showtabline = 2                         -- always show tabs
vim.opt.conceallevel = 0                        -- so that `` is visible in markdown files
vim.opt.shortmess:append "c"
vim.opt.signcolumn = "yes"                      -- always show the sign column, otherwise it would shift the text each time
vim.cmd [[set iskeyword+=-]]


-- Possible deletion
-- vim.cmd "set whichwrap+=<,>,[,],h,l"
-- vim.cmd [[set formatoptions-=cro]] -- TODO: this doesn't seem to work
