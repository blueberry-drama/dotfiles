-- Base
require "esmantovich.options" -- Neovim parameters
require "esmantovich.plugins"
require "esmantovich.keymaps"
require "esmantovich.treesitter"

require "esmantovich.telescope"

-- Comfy
require "esmantovich.colorscheme"
require 'colorizer'.setup() -- Color highlighter
require "esmantovich.autopairs"

require "esmantovich.lualine"

require "esmantovich.lsp"
require "esmantovich.null-ls"
