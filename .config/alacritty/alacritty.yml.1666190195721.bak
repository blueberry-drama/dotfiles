# Imports are loaded in order, skipping all missing files, with the importing
# file being loaded last. If a field is already present in a previous import, it
# will be replaced.
#
# All imports must either be absolute paths starting with `/`, or paths relative
# to the user's home directory starting with `~/`.
# import:
#   - ~/.config/alacritty/dracula.yml

# Any items in the `env` entry below will be added as
# environment variables. Some entries may override variables
# set by alacritty itself.
env:
  # TERM variable
  #
  # This value is used to set the `$TERM` environment variable for
  # each instance of Alacritty. If it is not present, alacritty will
  # check the local terminfo database and use `alacritty` if it is
  # available, otherwise `xterm-256color` is used.
  TERM: xterm-256color

window:
  dimensions:
    columns: 140
    lines: 38
  padding:
    x: 3
    y: 3

  decorations: none
  # Background opacity
  #
  # Window opacity as a floating point number from `0.0` to `1.0`.
  # The value `0.0` is completely transparent and `1.0` is opaque.
  # opacity: 0.9 

scrolling:
  # Maximum number of lines in the scrollback buffer.
  history: 10000
  # Scrolling distance multiplier.
  multiplier: 3

font:
  normal:
    # Font family
    #
    # Default:
    #   - (macOS) Menlo
    #   - (Linux/BSD) monospace
    #   - (Windows) Consolas
    family: GoMono Nerd Font Mono

    # The `style` can be specified to pick a specific face.
    style: Regular

  # Bold font face
  bold:
    # Font family
    #
    # If the bold family is not specified, it will fall back to the
    # value specified for the normal font.
    family: GoMono Nerd Font Mono

    # The `style` can be specified to pick a specific face.
    style: Bold

  # Italic font face
  italic:
    # Font family
    #
    # If the italic family is not specified, it will fall back to the
    # value specified for the normal font.
    family: GoMono Nerd Font Mono
    # The `style` can be specified to pick a specific face.
    style: Italic

  # Bold italic font face
  bold_italic:
    # Font family
    #
    # If the bold italic family is not specified, it will fall back to the
    # value specified for the normal font.
    family: GoMono Nerd Font Mono
    # The `style` can be specified to pick a specific face.
    style: Bold Italic

  size: 18.0
  # Offset is the extra space around each character. `offset.y` can be thought
  # of as modifying the line spacing, and `offset.x` as modifying the letter
  # spacing.
  #offset:
  #  x: 0
  #  y: 0
  # Glyph offset determines the locations of the glyphs within their cells with
  # the default being at the bottom. Increasing `x` moves the glyph to the
  # right, increasing `y` moves the glyph upward.
  #glyph_offset:
  #  x: 0
  #  y: 0
  # Use built-in font for box drawing characters.
  #
  # If `true`, Alacritty will use a custom built-in font for box drawing
  # characters (Unicode points 2500 - 259f).
  #
  #builtin_box_drawing: true

# If `true`, bold text is drawn using the bright color variants.
#draw_bold_text_with_bright_colors: false
# Colors (Gruvbox dark)
colors:
  name: Cloud
  author: ""
  primary:
    background: "#000000"
    foreground: "#ffffff"
  cursor:
    text: "#000000"
    cursor: "#ffffff"
  normal:
    black: "#222827"
    red: "#d5a8e3"
    green: "#9c75dd"
    yellow: "#9898ae"
    blue: "#654a96"
    magenta: "#625566"
    cyan: "#a9d1df"
    white: "#e6ebe5"
  bright:
    black: "#5d6f74"
    red: "#cd749c"
    green: "#63b0b0"
    yellow: "#c0c0dd"
    blue: "#5786bc"
    magenta: "#3f3442"
    cyan: "#849da2"
    white: "#d9d6cf"
theme: Cloud
