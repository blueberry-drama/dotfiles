# GNU/Linux system config files managed by GNU/Stow.


`stow .` is equivalent to:\
`stow --dir=~/.dotfiles --target=~/`\
default `--target directory` is a PARENT directory of the directory in which user executes `stow .`
