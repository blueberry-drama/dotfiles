(define-package "consult" "20221015.2101" "Consulting completing-read"
  '((emacs "27.1")
    (compat "28.1"))
  :commit "428ffd0365c11f543a5640385cd0bceb4a500854" :authors
  '(("Daniel Mendler and Consult contributors"))
  :maintainer
  '("Daniel Mendler" . "mail@daniel-mendler.de")
  :url "https://github.com/minad/consult")
;; Local Variables:
;; no-byte-compile: t
;; End:
