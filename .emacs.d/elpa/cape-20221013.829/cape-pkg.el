(define-package "cape" "20221013.829" "Completion At Point Extensions"
  '((emacs "27.1"))
  :commit "c976301286df7120863fe31b3224a7b22c8cf550" :authors
  '(("Daniel Mendler" . "mail@daniel-mendler.de"))
  :maintainer
  '("Daniel Mendler" . "mail@daniel-mendler.de")
  :url "https://github.com/minad/cape")
;; Local Variables:
;; no-byte-compile: t
;; End:
