; --- init ---
(require 'package)
(add-to-list 'package-archives '( "melpa" . "https://melpa.org/packages/") t)
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))     

(unless (package-installed-p 'base16-theme)
  (package-refresh-contents)
  (package-install 'base16-theme))

(org-babel-load-file (expand-file-name "~/.emacs.d/config.org"))

; --- custom-set --- 

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(display-line-numbers-type 'relative)
 '(package-selected-packages
   '(flycheck typescript-mode lsp-ui lsp-mode doom-modeline embark-consult embark consult marginalia orderless cape corfu vertico all-the-icons all-the-icon emacs-dashboard dashboard rainbow-delimiters sudo-edit expand-region rainbow-mode avy org-bullets which-key use-package smex base16-theme)))

; Настройка цвета line number для ashes темы 
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(line-number ((t (:background "#1c2023" :foreground "#747c84")))))

; TODO: Вынести в config.org
; ВОЗМОЖНО ЗАМЕНИТЬ НА HELM / AMX / PRESCIENT / IVY (SWIPER)

(global-set-key (kbd "M-X") 'smex-major-mode-commands)
; This is your old M-x.
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)

