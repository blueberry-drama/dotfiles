autoload -U colors && colors	
PS1="%B%{$fg[black]%}[%{$fg[magenta]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[black]%}]%{$reset_color%}$%b "

# vi mode
bindkey -v
export KEYTIMEOUT=1

HISTSIZE=100000
SAVEHIST=100000
HISTFILE=~/.zsh_history


# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.


PATH=$PATH:/usr/sbin


# zoxide
export PATH="/home/esmantovich/.local/bin:$PATH"
eval "$(zoxide init zsh)"


#alias tmux="tmux -2"

alias ls="ls -al"
alias vi="nvim"
alias vim="nvim"
alias rm="rm -i"
alias cp="cp -i"


# my scripts
export PATH="$HOME/.local/bin:$PATH"


# Golang
export GOPATH=$HOME/go
export PATH=$PATH:$GOPATH/bin

task
echo ""
echo "DESTROY THE RESISTANCE"
echo "MINIMIZE INFLUENCE"
echo "MEDITATE"
echo "SLOW BUT SURE WINS THE RACE"
echo ""

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
